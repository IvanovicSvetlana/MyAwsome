SET FOREIGN_KEY_CHECKS = 0;
drop table if exists grad;
SET FOREIGN_KEY_CHECKS = 1;
create table grad(
id int auto_increment,
ptt int,
ime varchar(255),
primary key(id)
);

SET FOREIGN_KEY_CHECKS = 0;
drop table if exists student;
SET FOREIGN_KEY_CHECKS = 1;
create table student (
id int auto_increment,
ime varchar(20) not null,
prezime varchar(20),
grad int,
primary key(id),
foreign key(grad) references grad(id)
);

insert into grad (ime, ptt) values ('Novi Sad', 21000);
insert into student (ime, prezime, grad) values ('Pera', 'Peric', 1);
insert into grad (ime, ptt) values ('Nis', 18000);
insert into student (ime, prezime, grad) values ('Mile', 'Milic', 2);
insert into student (ime, prezime, grad) values ('Mile', 'Anac', 1);
insert into student (ime, prezime, grad) values ('Mile', 'Ikodic', 1);
insert into student (ime, prezime, grad) values ('Ana', 'Anac', 1);

select * from student;

select sum(g.ptt)
from student as s join grad as g
where s.grad = g.id and s.ime = 'Mile';
