package rs.ac.uns.ftn.informatika.interfaces;

import rs.ac.uns.ftn.informatika.test.Connection;
import rs.ac.uns.ftn.informatika.test.ForwardedConnection;

public class Main {
	public static void main(String[] args) {
		
		// metoda equals je override-ovana u klase Connection
		Connection conn = new Connection("www.ftn.uns.ac.rs", 8080, 1);
		Connection conn2 = new Connection("www.ftn.uns.ac.rs", 8080, 1);
		
		// nasleđivanje klasa i polimorfizam
		ForwardedConnection fconn = new ForwardedConnection("www.ftn.uns.ac.rs", 8080, 1, 3456);
		ForwardedConnection fconn2 = new ForwardedConnection("www.ftn.uns.ac.rs", 8482, 1, 3456);
		
		Boolean jednakost = conn.equals(conn2) ? true : false;
		System.out.println("Testiramo jednakost konekcija: " + jednakost);
		
		Boolean jednakost2 = conn.equals(fconn) ? true :false; // ova konstrukcija se zove conditional operator
		Boolean jednakost3 = conn.equals(fconn2) ? true :false;
		System.out.println("Testiramo polimorfno poređenje: ");
		// objekat klase Connection i ForwardedConnection su jednaki jer druga klasa nasleđuje prvu
		System.out.println(jednakost2);
		System.out.println(jednakost3);

	}
}
