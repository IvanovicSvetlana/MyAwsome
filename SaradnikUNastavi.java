import java.util.ArrayList;


public class SaradnikUNastavi extends Asistent{

	double trenutniProsekNaosnovnimStudijama;
	private ArrayList<Integer> ocene;
	private double prosek;


	public SaradnikUNastavi(double trenutniProsekNaosnovnimStudijama) {
		
		this.trenutniProsekNaosnovnimStudijama = trenutniProsekNaosnovnimStudijama;
	}
	
	
public ArrayList<Integer> getOcene() {
		return ocene;
	}


	public void setOcene(ArrayList<Integer> ocene) {
		this.ocene = ocene;
	}


	public double getProsek() {
		return prosek;
	}


	public void setProsek(double prosek) {
		this.prosek = prosek;
	}


	public double getTrenutniProsekNaosnovnimStudijama() {
		return trenutniProsekNaosnovnimStudijama;
	}


public SaradnikUNastavi() {
super();	
}
	
		public double izracunajProsek() {
			if (this.listaPredmeta.size()>0) {
				int sumaOcena = 0;
				for (int i=0; i<this.ocene.size(); i++) {
					sumaOcena += this.ocene.get(i);
				}
				this.prosek = (double)sumaOcena/this.ocene.size();
			} else {
				System.out.println("Nije moguce izracunati prosek.");
			}
			return this.prosek;
		}
	public void setTrenutniProsekNaosnovnimStudijama(
			double trenutniProsekNaosnovnimStudijama) {
		this.trenutniProsekNaosnovnimStudijama = trenutniProsekNaosnovnimStudijama;
	}
	
}
