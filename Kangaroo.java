package rs.ac.uns.ftn.informatika.polymorphism;

public class Kangaroo implements Jumper {
	
	public String family;
	public Double weight;
	public Double height;
	
	public Kangaroo(Double weight,Double height) {
		super();
		this.family = "Macropodidae";
		this.weight = weight;
		this.height = height;
		
	}

	@Override
	public void jump() {
		System.out.println("An animal of family " + this.family + " has jumped high with its " +
							this.weight + " pound of weight + " has jumped high with its " + this.height + " cm height " );
	}

}
