package rs.ac.uns.ftn.informatika.interfaces;

import java.util.ArrayList;

import rs.ac.uns.ftn.informatika.test.Connection;

public interface Runable {
	// metode interfejsa mogu biti samo public
	// jer je namena interfejsa kao elementa programskog jezika Java takva
	public void run();
	public String signIt(Integer signingOrder);
	public ArrayList<String> runnedTasks();
	public ArrayList<Connection> getUtilizedConnections();
	/* komentar*/
}
