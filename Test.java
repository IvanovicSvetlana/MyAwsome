package rs.ac.uns.ftn.informatika.test;

import java.util.ArrayList;

public class Test {
	
	String name;
	ArrayList<String> tasks;
	Connection connection;

	public Test(ArrayList<String> tastks, String name, Connection connection) {
		super();
		this.tasks = tastks;
		this.name = name;
		this.connection = connection;
	}

	public ArrayList<String> getTastks() {
		return tasks;
	}
	
	public String getName() {
		return name;
	}
	
	public Connection getConnection() {
		return connection;
	}

	public void setTastks(ArrayList<String> tastks) {
		this.tasks = tastks;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public String toString() {
		return name + " [tastks=" + tasks + " conneciton=" + connection + "]";
	}

}
